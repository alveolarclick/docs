---
title: Troubleshooting
---

## `general.useragent.override` doesn't work

`privacy.resistFingerprinting` overrides the useragent, setting it to a “generic” value.

After setting `privacy.resistFingerprinting` to `false`, the value in `general.useragent.override` will be honoured.
