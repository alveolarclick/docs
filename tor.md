---
title: Tor
---

**Note: It is not recommended to use Tor with current builds of LibreWolf**

We do not recommend connecting over Tor on LibreWolf at the moment. Please use the Tor browser for the time being.
