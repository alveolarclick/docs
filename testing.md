---
title: Browser Tests
---

## Security/Fingerprint

`privacy.resistFingerprinting` is relatively new; please give it some time to be more widely used and thus less finger-printable. If you are using an online tool to analyze your browser make sure to read and understand what the test is about.

- [SSLLabs](https://www.ssllabs.com/ssltest/viewMyClient.html)
- [AmiUnique](https://amiunique.org/fp)
- [BrowserLeaks](https://browserleaks.com/)
- [FingerPrintJS2](https://valve.github.io/fingerprintjs2/)
- [Third-Party-Cookies](https://alanhogan.github.io/web-experiments/3rd/third-party-cookies.html)
- [Testing-Notifications](https://www.bennish.net/web-notifications.html)
- [Browser-Storage-Abuser](https://demo.agektmr.com/storage/)
- [Service-Workers-Push-Test](https://gauntface.github.io/simple-push-demo/)

## Performances

Performance tests can be done with [Octane](https://chromium.github.io/octane/). It needs to be launched with other applications closed and with no other activity. It is recommended to run the test multiple times and then calculate the average.

## DNS/VPN/Proxy Leak

- [IPLeak](https://ipleak.net/)
- [Tenta-Test](https://tenta.com/test/)
- [IP-Browserleaks](https://browserleaks.com/ip)
