---
title: Settings Docs
---

Complete and up-to-date settings documentation is coming soon. Meanwhile, check out the [settings repository](https://gitlab.com/librewolf-community/settings) for information about the settings that power LibreWolf.

Feel free to create issues or merge requests if you find bugs or wish to propose enhancements.
